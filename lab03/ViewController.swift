//
//  ViewController.swift
//  MyFifthApp
//
//  Created by admin on 1/27/17.
//  Copyright © 2017 Eduardo Aguilera. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var output: UITextView!
    
    @IBOutlet weak var input: UITextField!
    
    @IBAction func tapButton(_ sender: Any) {
        getString(var1: input.text!) { (result) in
            DispatchQueue.main.async(execute:{
                self.output.text = result
            })
        }
    }
    

    func getString(var1: String, completion: @escaping (_ result: String)-> Void) -> Void{
        let myURL = URL(string: "http://date.jsontest.com/")
        var request = URLRequest(url: myURL!)
        request.httpMethod = "GET"
        let postString = "thing1=\(var1)&"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in
            if error != nil {
                print("error \(error)")
                return
            }
            if let response = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                completion(response as String)
            }
            else
            {
                completion("some error occured")
            }
        })
        task.resume()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

