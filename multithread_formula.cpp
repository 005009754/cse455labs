#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <iostream>
#include <SDL/SDL_thread.h>
#include <vector>

using namespace std;

int mA[3][2]={{1,2},{5,8},{7,12}};
int mB[2][3]={{6,10,15},{3,14,0}};
int mC[3][3]={{0,0,0},{0,0,0},{0,0,0}};

class m{
  public:
   void pA(int m[][2]);
   void pB(int m[][3]);
   void pC(int m[][3]);
  private:
   int row;
   int col;
   int pro;
};

int dProduct(void *data)
{
  int row;
  int col;
  int pro;
  char *threadname;
  threadname = (char *) data;
  cout<<"its: "<<threadname<<endl;
  for(row =0;row<3;row++)
  {
  for(col=0;col<3;col++){
      for(pro=0;pro<2;pro++)
      {
      mC[row][col]+=mA[row][pro]*mB[pro][col];
      }
  }
}
  return 0;
}

void m::pA(int m[][2])
{
  cout<<"Matrix a ==> "<<endl;
  for(row=0;row<3;row++)
  {
  for(col=0;col<2;col++)
     {
     cout<<mA[row][col]<<" ";cout<<endl;
     }
  }
  cout<<endl;
}

void m::pB(int m[][3])
{
  cout<<"Matrix b ==> "<<endl;
  for(row=0;row<2;row++)
  {
  for(col=0;col<3;col++)
    {
    cout<<mB[row][col]<<" ";cout<<endl;
    }
  }
  cout<<endl;
}

void m::pC(int m[][3])
{
  cout<<"Matrix c ==> "<<endl;
  for(row=0;row<3;row++)
  {
    for(col=0;col<3;col++)
    {
    cout<<mC[row][col]<<" ";cout<<endl;
    }
  }
  cout<<endl;
}

int main()
{ 
  m matricess;
  SDL_Thread *sumThread;
  sumThread = SDL_CreateThread(dProduct,(void *) " dot product");
  if(sumThread == NULL)
  {
    cout<<"\nSDL erroe: "<<SDL_GetError()<<endl;}
  else
  {
    int returnValue;
    SDL_WaitThread(sumThread, &returnValue);
    cout<<"Dot product of a & b==> "<<endl;
    matricess.pA(mA);
    matricess.pB(mB);
    cout<<"Equal matrix c==> "<<endl;
    matricess.pC(mC);
    cout<<endl;
  }

return 0;
}
